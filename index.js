const express = require('express');
const app = express();
const cors = require('cors')
const mongoose = require('mongoose');

const Data = require('./model/data')

let port = process.env.PORT || 3000;

app.use(cors())
app.use(express.urlencoded({extended: true})); 
app.use(express.json());  


mongoose.connect('mongodb+srv://admin:admin@cluster0.pvdun.mongodb.net/test', 
{
  useNewUrlParser: true,
  useUnifiedTopology: true 
}).then(()=>{
  console.log("Connected to db")
}).catch(e=>{
  console.log("No Connection"+ e)
})

app.get('/',(req,res)=>{
    res.send("IONIC APP DATA")
})



app.post('/data',async(req,res)=>{

  try{
    const addNewPost = new Data(req.body);
    // console.log(req.body)
    const insertPost = await addNewPost.save();
    res.send(insertPost)
  }catch(e){
    res.send(e)
  }
 
})

app.get('/datas',async(req,res)=>{
    let data = await Data.find({})
    res.send(data)
})

app.listen(port,(req,res)=>{
    console.log(`Listening to port ${port}`) 
})