
const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const projectSchema= new Schema({
    Date:{
        type:String,
    },
    ReferenceNo:{
        type:Number,
    },
    Counselor:{
        type:String,
    },
    Method:{
        type:String,
    },
    Years:{
        type:String,
    },
    Months:{
        type:String,
    },
    Amount:{
        type:String,
    },
    Balance:{
        type:String,
    },

});
const data = mongoose.model('data',projectSchema);
module.exports = data